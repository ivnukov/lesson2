import datetime
import random
import traceback


def sorted_and_unique(values):
    """
    Function receive the list and returns sorted list of unique values
    :param values: list of numbers
    :return: sorted list of unique values
    :rtype: list
    """
    return sorted(list(set(values)))


def totally_unique(func_to_wrap):
    def wrapper(*args, **kwargs):
        length = kwargs.get('length') if len(args) == 0 else args[0]
        result = []
        times_called = 0
        while len(result) < length:
            result = sorted_and_unique(func_to_wrap(*args, **kwargs))
            times_called += 1
        print("I have to call function", times_called, "times!")
        return result

    return wrapper


@totally_unique
def generate_random_list(length):
    """
    Generate list with random values
    :param length: length of output list
    :return: list of random numbers and characters
    """
    values = [int(random.randint(0, length * 10)) for x in range(length)]
    return values


def logger_boy(func_to_wrap):
    def wrapper(*args, **kwargs):
        try:
            return func_to_wrap(*args, **kwargs)
        except Exception as err:
            with open('logs.txt', 'a') as f:
                message = '{}    {}:\n {} \n\n'.format(
                    datetime.datetime.now(),
                    err.__class__.__name__,
                    traceback.format_exc()
                )
                f.write(message)

    return wrapper


@logger_boy
def divider(first, second):
    return first / second


def choicer():
    return random.choice([0, 2, 7])


if __name__ == '__main__':
    unique_list = generate_random_list(30)
    unique_list_randomly_divided = [divider(x, choicer()) for x in unique_list]
    print(unique_list_randomly_divided)
    print(len([x for x in unique_list_randomly_divided if x is None]))
