class A:
    p = 3


class B:
    def __init__(self, num):
        self.p = list(range(num))

    def __lt__(self, other):
        return len(self.p) < len(other.p)

    def __gt__(self, other):
        return len(self.p) > len(other.p)

    def __eq__(self, other):
        print(len(self.p))
        print(len(other.p))
        return len(self.p) == len(other.p)

    def __add__(self, other):
        prog = B(0)
        prog.p = self.p + other.p
        return prog

b1 = B(2)
b2 = B(3)

print(b1 == b2)
print(b1 < b2)
print(b1 > b2)

print((b1 + b2).p)


class A:
    pass


a = A()
a.prop = 15
print(a.prop)
