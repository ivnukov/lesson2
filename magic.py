class MagicMethodClass:
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return "I am {}".format(self.name)

    def __repr__(self):
        return "Custom repr of object {} - {}. Id is {}".format(
            self.__module__,
            type(self).__name__,
            hex(id(self))
        )

    def __del__(self):
        print('I feel bad Mr. Stark')

    def __call__(self, first, second):
        return first * second


a = MagicMethodClass('12345')

print(a(7, 8))

