from csv import DictReader
from datetime import date

import requests


class Employee:
    def __init__(self, first_name, last_name, email=None, technologies=None, main_skill=None, main_skill_grade=None):
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.technologies = technologies
        self.main_skill = main_skill
        self.main_skill_grade = main_skill_grade

    @property
    def full_name(self):
        return f"{self.first_name} {self.last_name}"

    def __str__(self):
        return self.full_name

    def __repr__(self):
        return str(self.__dict__)

    @staticmethod
    def get_workdays():
        now = date.today()
        month_start = date(now.year, now.month, 1)
        weekend = [5, 6]
        diff = (now - month_start).days + 1
        day_count = len([x for x in range(diff) if x not in weekend])
        return day_count

    @classmethod
    def from_string(cls, employee_string):
        names = employee_string.split(" ", 1)
        if len(names) < 2:
            raise ValueError("Both first and last names required.")
        return cls(first_name=names[0], last_name=names[1], )

    @classmethod
    def from_csv(cls, fp=None, url=None):
        obj_list = []
        if fp is not None and url is not None:
            raise ValueError("Only fp or url is allowed.")
        if url is not None:
            file_data = DictReader(
                requests.get(
                    url
                ).text.split("\n"))
        if fp is not None:
            with open(fp, 'r') as f:
                file_data = [row for row in DictReader(f)]
        for row in file_data:
            first_name, last_name = row['Full Name'].split(" ", 1)
            email = row['Email']
            technologies = row['Technologies'].split('|')
            main_skill = row['Main Skill']
            main_skill_grade = row['Main Skill Grade']
            obj_list.append(cls(first_name=first_name,
                                last_name=last_name,
                                email=email,
                                technologies=technologies,
                                main_skill=main_skill,
                                main_skill_grade=main_skill_grade))
        return obj_list


if __name__ == "__main__":
    me = Employee(first_name="Igor", last_name="Vnukov")
    print(me.full_name)
    print(me.get_workdays())
    print(Employee.get_workdays())
    newMe = Employee.from_string('Robert Downey JR')
    print(newMe.full_name)
    print(Employee.from_csv(fp='candidates.csv'))
    print(Employee.from_csv(url='https://bitbucket.org/ivnukov/lesson2/raw/master/candidates.csv'))
