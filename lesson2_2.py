class Parent:
    def __init__(self, property_1, property_2):
        self.property_1 = property_1
        self.property_2 = property_2

    def print_place(self):
        print('Called from parent.')


class Child(Parent):
    def __init__(self, property_1, property_2, child_property):
        super().__init__(property_1, property_2)
        self.child_property = child_property

    def print_place(self):
        super().print_place()
        print('From child')
