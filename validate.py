def validate(emp_list):
    if len(set([x.email.lower() for x in emp_list])) < len(emp_list):
        raise ValueError


class Employee:
    def __init__(self, email):
        self.validate(email)
        self.email = email
        self.save_email_to_file()

    def validate(self, mail):
        print(self.get_emails_from_file())
        if mail in self.get_emails_from_file():
            raise ValueError

    def save_email_to_file(self):
        with open('emails', 'a') as f:
            f.write(self.email)
            f.write('\n')

    def get_emails_from_file(self):
        with open('emails', 'a+') as f:
            f.seek(0)
            data = f.read()
        return data.split('\n')

emp1 = Employee('123')
emp2 = Employee('qweqweqw00')
emp3 = Employee('123')
